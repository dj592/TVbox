# classes.dex

.class public final synthetic Lcom/androidx/newz21;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final synthetic a:Lcom/androidx/h31;


# direct methods
.method public synthetic constructor <init>(Lcom/androidx/h31;)V
    .registers 3

    iput-object p1, p0, Lcom/androidx/newz21;->a:Lcom/androidx/h31;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final Autoupdate()V
    .registers 7

    const-string v4, "内置仓库"

    const-string v5, "www.baidu.com"

    .line 3
    iget-object v0, p0, Lcom/androidx/newz21;->a:Lcom/androidx/h31;

    new-instance v2, Lcom/androidx/ot0;

    .line 103
    const/16 v3, 0xe

    .line 105
    invoke-direct {v2, v3}, Lcom/androidx/ot0;-><init>(I)V

    .line 108
    new-instance v3, Lcom/github/tvbox/osc/bean/MoreSourceBean;

    .line 110
    invoke-direct {v3}, Lcom/github/tvbox/osc/bean/MoreSourceBean;-><init>()V

    .line 113
    invoke-virtual {v3, v4}, Lcom/github/tvbox/osc/bean/MoreSourceBean;->setSourceName(Ljava/lang/String;)V

    .line 116
    invoke-virtual {v3, v5}, Lcom/github/tvbox/osc/bean/MoreSourceBean;->setSourceUrl(Ljava/lang/String;)V

    .line 119
    iput-object v3, v2, Lcom/androidx/ot0;->obj:Ljava/lang/Object;

    .line 121
    invoke-virtual {v0, v2}, Lcom/androidx/h31;->handleRemotePush(Lcom/androidx/ot0;)V

    .line 124
    return-void
.end method






#这个方法放在代码HomeActivity里
#判断csp.jar是否存在，如果不存在则执行Autoupdate()方法
.method private checkCSPJarFileExistence()V
    .registers 5

    invoke-virtual {p0}, Lcom/github/tvbox/osc/ui/activity/HomeActivity;->getFilesDir()Ljava/io/File;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    const-string v2, "csp.jar"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1e

    new-instance v0, Lcom/androidx/h31;

    invoke-direct {v0, p0}, Lcom/androidx/h31;-><init>(Landroid/app/Activity;)V

    new-instance v1, Lcom/androidx/newz21;

    invoke-direct {v1, v0}, Lcom/androidx/newz21;-><init>(Lcom/androidx/h31;)V

    invoke-virtual {v1}, Lcom/androidx/newz21;->Autoupdate()V

    :cond_1e
    return-void
.end method




#这个放在HomeActivity的init()V
#调用执行checkCSPJarFileExistence()方法
 invoke-direct {p0}, Lcom/github/tvbox/osc/ui/activity/HomeActivity;->checkCSPJarFileExistence()V
